Avaliacao::Application.routes.draw do
  resources :relatorios, only: [:index]
  resources :consultores, only: [:index]
  root to: "consultores#index"
end
