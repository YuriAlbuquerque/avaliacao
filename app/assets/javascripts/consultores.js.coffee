# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
        addRowToTable = (parent, colspans, texts) ->
                row = $('<tr>')
                for i in [0..texts.length-1]
                        $('<td>').text(texts[i])
                                .attr("colspan", colspans[i])
                                .appendTo(row)
                row.appendTo(parent)

        round = (n) ->
                Math.round(n*100)/100
        $("select#usuarios_co_usuario").change ->
                url = "/relatorios.json?usuarios[co_usuario][]="
                selected = $("select option:selected")
                url += "&usuarios[co_usuario][]="+option.value for option in selected

                $("a").each ->
                        $(this).attr("href", url)

        relatorioCallback = (xhr, data, status) ->
                $('div#output').empty()
                records = eval('(' + data.responseText + ')')
                cont = 0
                for record in records
                        if cont > 0
                                table = $('<table>')
                                thead = $('<thead>')
                                tbody = $('<tbody>')
                                tfoot = $('<tfoot>')

                                # Name
                                addRowToTable(thead, [5], [record.no_usuario])

                                # Header
                                addRowToTable(thead, [1, 1, 1, 1, 1],
                                        ["Período", "Receita Líquida", "Custo Fixo", "Comissão", "Lucro"])

                                rc_total = 0
                                cf_total = 0
                                com_total = 0
                                luc_total = 0

                                for i in [0..record.dates.length-1]
                                        addRowToTable(tbody, [1, 1, 1, 1, 1],
                                                [record.dates[i], round(record.receita_liquida[i]),
                                                round(record.custo_fixo[i]), round(record.comissao[i]),
                                                round(record.lucro[i])])
                                        rc_total += record.receita_liquida[i]
                                        cf_total += record.custo_fixo[i]
                                        com_total += record.comissao[i]
                                        luc_total += record.lucro[i]

                                addRowToTable(tfoot, [1, 1, 1, 1, 1],
                                        ["Saldo", round(rc_total), round(cf_total), round(com_total),
                                        round(luc_total)])

                                thead.appendTo(table)
                                tbody.appendTo(table)
                                tfoot.appendTo(table)
                                table.appendTo($('div#output'))
                        cont++

        graficoCallback = (xhr, data, status) ->
                $('div#output').empty()
                records = eval('(' + data.responseText + ')')
                cont = 0
                table = $('<table>')
                $('<caption>').text("Gráfico")
                        .appendTo(table)

                header = $('<thead>').appendTo(table)

                trHeader = $('<tr>')
                $('<td>').appendTo(trHeader)
                trHeader.appendTo(header)

                tbody = $('<tbody>')
                tbody.appendTo(table)

                for date in records[0].dates
                        $('<th>').text(date)
                                .attr("scope", "col")
                                .appendTo(trHeader)
                for record in records
                        if cont >0
                                row = $('<tr>')
                                $('<th>').text(record.no_usuario)
                                        .attr("scope", "row")
                                        .appendTo(row)

                                for lucro in record.lucro
                                        $('<td>').text(round(lucro)).appendTo(row)

                                row.appendTo(tbody)
                        cont++

                table.appendTo($('div#output'))
                table.visualize()

        pizzaCallback = (xhr, data, status) ->
                $('div#output').empty()
                records = eval('(' + data.responseText + ')')
                cont = 0
                table = $('<table>')
                $('<caption>').text("Gráfico em Pizza")
                        .appendTo(table)

                header = $('<thead>').appendTo(table)

                trHeader = $('<tr>')
                $('<td>').appendTo(trHeader)
                trHeader.appendTo(header)

                tbody = $('<tbody>')
                tbody.appendTo(table)

                for date in records[0].dates
                        $('<th>').text(date)
                                .attr("scope", "col")
                                .appendTo(trHeader)

                lucro_total = 0
                for record in records
                        for lucro in record.lucro
                                lucro_total += lucro
                for record in records
                        if cont >0
                                row = $('<tr>')
                                $('<th>').text(record.no_usuario)
                                        .attr("scope", "row")
                                        .appendTo(row)

                                for lucro in record.lucro
                                        $('<td>').text(round(100*lucro/lucro_total)).appendTo(row)

                                row.appendTo(tbody)
                        cont++

                table.appendTo($('div#output'))
                table.visualize({type: "pie"})

        $("#relatorio").bind "mousedown", ->
                $("form").bind("ajax:complete", relatorioCallback)
        $("#grafico").bind "mousedown", ->
                $("form").bind("ajax:complete", graficoCallback)
        $("#pizza").bind "mousedown", ->
                $("form").bind("ajax:complete", pizzaCallback)
