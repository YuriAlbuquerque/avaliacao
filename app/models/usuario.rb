class Usuario < ActiveRecord::Base
  attr_accessible :no_usuario
  self.table_name = "cao_usuario"
  self.primary_key = "co_usuario"

  def self.consultores
    return Usuario.joins("LEFT JOIN permissao_sistema AS ps " +
                        "ON ps.co_usuario=cao_usuario.co_usuario").where("ps.co_sistema=1 AND in_ativo='S' AND " +
                                                                         "(CO_TIPO_USUARIO=0 " +
                                                                         "OR CO_TIPO_USUARIO=1 " +
                                                                         "OR CO_TIPO_USUARIO=2)")
  end

  def receita_liquida(data_min, data_max)
    totals = []
    timespans = (data_min..data_max).select { |d| d.day == 1 }
    timespans.each do |current|
      totals << Usuario.joins("LEFT JOIN cao_os AS co ON co.co_usuario=cao_usuario.co_usuario")
        .joins("LEFT JOIN cao_fatura AS cf ON co.co_os=cf.co_os")
        .where("cao_usuario.co_usuario = ? AND cf.data_emissao>=? AND cf.data_emissao<?", self.co_usuario, current, current+1.month)
        .sum("(cf.valor - (cf.total_imp_inc*cf.valor/100))").to_f
    end
    return totals
  end

  def custo_fixo(data_min, data_max)
    timespans = (data_min..data_max).select { |d| d.day == 1 }
    c_f = [Usuario.joins("LEFT JOIN cao_salario AS cs ON cs.co_usuario=cao_usuario.co_usuario")
             .where("cao_usuario.co_usuario = ?", self.co_usuario)
             .sum("brut_salario").to_f]
    c_f = c_f*timespans.length
    return c_f
  end

  def comissao(data_min, data_max)
    totals = []
    timespans = (data_min..data_max).select { |d| d.day == 1 }
    timespans.each do |current|
      totals << Usuario.joins("LEFT JOIN cao_os AS co ON co.co_usuario=cao_usuario.co_usuario")
        .joins("LEFT JOIN cao_fatura AS cf ON co.co_os=cf.co_os")
        .where("cao_usuario.co_usuario = ? AND cf.data_emissao>=? AND cf.data_emissao<?", self.co_usuario, current, current+1.month)
        .sum("(cf.valor - (cf.total_imp_inc*cf.valor/100))*cf.comissao_cn/100").to_f
    end
    return totals
  end

  def lucro(data_min, data_max)
    totals = []
    timespans = (data_min..data_max).select { |d| d.day == 1 }
    r_l = receita_liquida(data_min, data_max)
    c_f = custo_fixo(data_min, data_max)
    com = comissao(data_min, data_max)
    i = 0
    timespans.each do |current|
      totals << r_l[i] - (c_f[i] + com[i])
      i += 1
    end

    return totals
  end

  def as_json(options = {})
    json = super(options)
    data_min = Date.new(options["data_min(1i)"].to_i,options["data_min(2i)"].to_i,options["data_min(3i)"].to_i)
    data_max = Date.new(options["data_max(1i)"].to_i,options["data_max(2i)"].to_i,options["data_max(3i)"].to_i)
    json['receita_liquida'] = receita_liquida(data_min, data_max)
    json['custo_fixo'] = custo_fixo(data_min, data_max)
    json['comissao'] = comissao(data_min, data_max)
    json['lucro'] = lucro(data_min, data_max)
    json['dates'] = (data_min..data_max).select { |d| d.day == 1 }

    return json
  end
end
