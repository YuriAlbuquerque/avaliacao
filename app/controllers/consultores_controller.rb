class ConsultoresController < ApplicationController
  def index
    @consultores = Usuario.consultores

    respond_to do |format|
      format.html
    end
  end
end
