class RelatoriosController < ApplicationController
  def index
    @usuarios = Usuario.all(conditions: {cao_usuario: params[:usuarios]})

    respond_to do |format|
      format.json { render json: @usuarios.to_json(params[:datas]) }
    end
  end
end
